package mvc.view;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import jaxb.board.dialog.Button;
import jaxb.board.dialog.Dialog;
import jaxb.board.dialog.Label;
import jaxb.board.dialog.Slider;
import jaxb.board.dialog.TextField;
import mvc.controller.SwingUtils;
import mvc.model.StaticData;

public class CreateDialog {

    public static JButton createComponent(Button button) {
        JButton jButton = new JButton();
        if (button.getBackground() != null) {
            jButton.setBackground(SwingUtils.createColor(button.getBackground()));
        }
        jButton.setHorizontalTextPosition(button.getHorizontalTextPosition());

        if (button.getLocation() != null) {
            jButton.setLocation(SwingUtils.createPoint(button.getLocation()));
        }
        if (button.getSize() != null) {
            jButton.setSize(SwingUtils.createDimension(button.getSize()));
        }

        if (button.getMargin() != null) {
            jButton.setMargin(SwingUtils.createInset(button.getMargin()));
        }
        if (button.getText() != null) {
            jButton.setText(button.getText());
        }

        StaticData.MAP_COMP.put(button.getName(), jButton);
        return jButton;
    }

    public static JDialog createComponent(Dialog dialog) {
        JDialog jDialog = new JDialog();
        jDialog.setSize(SwingUtils.createDimension(dialog.getSize()));
        jDialog.setDefaultCloseOperation(dialog.getDefaultCloseOperation());

        if (dialog.getLayout() != null) {
            jDialog.setLayout(SwingUtils.createLayout(dialog.getLayout()));
        } else {
            jDialog.setLayout(null);
        }

        jDialog.setTitle(dialog.getTitle());
        jDialog.setResizable(dialog.isResizable());
        return jDialog;
    }

    public static JLabel createComponent(Label label) {
        JLabel jLabel = new JLabel(label.getText());
        jLabel.setLocation(SwingUtils.createPoint(label.getLocation()));
        jLabel.setSize(SwingUtils.createDimension(label.getSize()));

        if (label.getFont() != null) {
            jLabel.setFont(SwingUtils.createFont(label.getFont()));
        }
        if (label.getForeground() != null) {
            jLabel.setForeground(SwingUtils.createColor(label.getForeground()));
        }

        jLabel.setHorizontalAlignment(label.getHorizontalAlignment());
        StaticData.MAP_COMP.put(label.getName(), jLabel);
        return jLabel;
    }

    public static void createComponent(Slider slider) {
        StaticData.slider = new JSlider(slider.getMinimum(), slider.getMaximum());
        StaticData.slider.setLocation(SwingUtils.createPoint(slider.getLocation()));
        StaticData.slider.setSize(SwingUtils.createDimension(slider.getSize()));

        StaticData.slider.setMajorTickSpacing(slider.getMajorTickSpacing());
        StaticData.slider.setPaintLabels(slider.isPaintLabels());
        StaticData.slider.setPaintTicks(slider.isPaintTicks());
        StaticData.slider.setVisible(false);
    }

    public static JTextField createComponent(TextField textField) {
        JTextField jTextField = new JTextField();
        jTextField.setLocation(SwingUtils.createPoint(textField.getLocation()));
        jTextField.setSize(SwingUtils.createDimension(textField.getSize()));

        StaticData.MAP_COMP.put(textField.getName(), jTextField);
        return jTextField;
    }
}
