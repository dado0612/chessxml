package mvc.view;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import jaxb.board.frame.Frame;
import jaxb.board.frame.RadioButton;
import jaxb.board.frame.ScrollPane;
import jaxb.board.panel.Panel;
import mvc.controller.SwingUtils;
import mvc.listener.ListListener;
import mvc.model.StaticData;

public class CreateFrame {

    public static JFrame createComponent(Frame frame) {
        JFrame jFrame = new JFrame(frame.getTitle());
        jFrame.setSize(SwingUtils.createDimension(frame.getSize()));
        jFrame.setDefaultCloseOperation(frame.getDefaultCloseOperation());

        if (frame.getLayout() != null) {
            jFrame.setLayout(SwingUtils.createLayout(frame.getLayout()));
        } else {
            jFrame.setLayout(null);
        }

        jFrame.setResizable(frame.isResizable());
        return jFrame;
    }

    public static JRadioButton createComponent(RadioButton radioButton,
            ButtonGroup group) {
        JRadioButton jRadioButton = new JRadioButton(radioButton.getText());
        jRadioButton.setFont(SwingUtils.createFont(radioButton.getFont()));
        jRadioButton.setHorizontalAlignment(radioButton.getHorizontalAlignment());

        group.add(jRadioButton);
        StaticData.MAP_COMP.put(radioButton.getName(), jRadioButton);
        return jRadioButton;
    }

    public static JScrollPane createComponent(ScrollPane scrollPane) {
        JList jList = new JList(new DefaultListModel());
        jList.addMouseListener(new ListListener(jList));
        jList.setSelectionMode(scrollPane.getList().getSelectionMode());

        JScrollPane jScrollPane = new JScrollPane(jList);
        jScrollPane.setLocation(SwingUtils.createPoint(scrollPane.getLocation()));
        jScrollPane.setSize(SwingUtils.createDimension(scrollPane.getSize()));

        StaticData.MAP_COMP.put(scrollPane.getName(), jScrollPane);
        StaticData.LIST_VIEW.add(jList);
        return jScrollPane;
    }

    public static JPanel createComponent(Panel panel) {
        JPanel jPanel = new JPanel();
        jPanel.setLocation(SwingUtils.createPoint(panel.getLocation()));
        jPanel.setSize(SwingUtils.createDimension(panel.getSize()));

        if (panel.getLayout() != null) {
            jPanel.setLayout(SwingUtils.createLayout(panel.getLayout()));
        } else {
            jPanel.setLayout(null);
        }
        return jPanel;
    }
}
