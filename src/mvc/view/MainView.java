package mvc.view;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import jaxb.board.frame.Frame;
import jaxb.board.frame.RadioButton;
import jaxb.board.panel.Panel;
import jaxb.board.panel.PanelBoard;
import jaxb.board.panel.PanelMatch;
import jaxb.board.panel.PanelMenu;
import mvc.controller.JAXBParser;
import mvc.listener.ContinueListener;
import mvc.listener.MoveListener;
import mvc.listener.ReplayListener;
import mvc.listener.StartListener;
import mvc.model.StaticData;

public class MainView {

    private static void setLookAndFeel() {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if (info.getName().equals("Nimbus")) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }

    private static JPanel generatePanel(PanelBoard board, JFrame parent) {
        String[] arr = {"A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8",
            "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8",
            "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8",
            "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8",
            "E1", "E2", "E3", "E4", "E5", "E6", "E7", "E8",
            "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8",
            "G1", "G2", "G3", "G4", "G5", "G6", "G7", "G8",
            "H1", "H2", "H3", "H4", "H5", "H6", "H7", "H8"};

        JPanel jPanel = CreateFrame.createComponent(new Panel(board));
        for (int i = 0; i < board.getButton().size(); i++) {
            JButton jButton = CreateDialog.createComponent(board.getButton().get(i));

            jButton.addActionListener(new MoveListener(arr[i], jButton));
            StaticData.CHESS_BOARD.get(i / 8).add(jButton);
            jPanel.add(jButton);
        }

        parent.add(jPanel);
        return jPanel;
    }

    private static JPanel generatePanel(PanelMatch match, JFrame parent) {
        JPanel jPanel = CreateFrame.createComponent(new Panel(match));
        for (int i = 0; i < match.getLabel().size(); i++) {
            JLabel jLabel = CreateDialog.createComponent(match.getLabel().get(i));

            if (i < 3) {
                StaticData.LABEL_NAME.add(jLabel);
            } else {
                StaticData.LABEL_THINK.add(jLabel);
            }
            jPanel.add(jLabel);
        }

        CreateDialog.createComponent(match.getSlider());
        jPanel.add(StaticData.slider);
        match.getScrollPane().forEach((pane) -> {
            jPanel.add(CreateFrame.createComponent(pane));
        });

        parent.add(jPanel);
        return jPanel;
    }

    private static JPanel generatePanel(PanelMenu menu, JFrame parent) {
        JPanel jPanel = CreateFrame.createComponent(new Panel(menu));
        ButtonGroup group = new ButtonGroup();

        for (int i = 0; i < menu.getRadioButton().size(); i++) {
            RadioButton radioButton = menu.getRadioButton().get(i);
            JRadioButton jRadioButton = CreateFrame.createComponent(radioButton, group);

            switch (i) {
                case 0:
                    jRadioButton.addActionListener(new StartListener(parent));
                    break;
                case 1:
                    jRadioButton.addActionListener(new ContinueListener(parent));
                    break;
                case 2:
                    jRadioButton.addActionListener(new ReplayListener(parent));
            }
            jPanel.add(jRadioButton);
        }

        parent.add(jPanel);
        return jPanel;
    }

    public static void main(String[] args) {
        setLookAndFeel();
        Frame frame = JAXBParser.readFrame();
        JFrame jFrame = CreateFrame.createComponent(frame);

        JPanel board = generatePanel(frame.getPanelBoard(), jFrame);
        board.setBorder(new SoftBevelBorder(BevelBorder.RAISED));
        JPanel match = generatePanel(frame.getPanelMatch(), jFrame);
        match.setBorder(BorderFactory.createEtchedBorder());

        JPanel menu = generatePanel(frame.getPanelMenu(), jFrame);
        menu.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
        jFrame.setVisible(true);
    }
}
