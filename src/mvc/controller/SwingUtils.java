package mvc.controller;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import javax.swing.JButton;
import mvc.model.StaticData;

public class SwingUtils {

    public static void updateCell(JButton button, String piece) {
        button.setToolTipText(piece);
        button.setIcon(StaticData.MAP_PIECE.get(piece));
    }

    public static Color createColor(String msg) {
        String[] arr = msg.split("[,]");
        return (new Color(Integer.valueOf(arr[0]), Integer.valueOf(arr[1]),
                Integer.valueOf(arr[2])));
    }

    public static Dimension createDimension(String msg) {
        String[] arr = msg.split("[,]");
        return (new Dimension(Integer.valueOf(arr[0]), Integer.valueOf(arr[1])));
    }

    public static Font createFont(String msg) {
        String[] arr = msg.split("[,]");
        return (new Font(arr[0], Integer.valueOf(arr[1]), Integer.valueOf(arr[2])));
    }

    public static Insets createInset(String msg) {
        String[] arr = msg.split("[,]");
        return (new Insets(Integer.valueOf(arr[0]), Integer.valueOf(arr[1]),
                Integer.valueOf(arr[2]), Integer.valueOf(arr[3])));
    }

    public static GridLayout createLayout(String msg) {
        String[] arr = msg.split("[,]");
        return (new GridLayout(Integer.valueOf(arr[0]), Integer.valueOf(arr[1])));
    }

    public static Point createPoint(String msg) {
        String[] arr = msg.split("[,]");
        return (new Point(Integer.valueOf(arr[0]), Integer.valueOf(arr[1])));
    }
}
