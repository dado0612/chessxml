package mvc.controller;

import javax.swing.JLabel;
import mvc.model.StaticData;

public class ThinkThread extends Thread {

    private int cnt = 0;

    @Override
    public void run() {
        try {
            while (true) {
                JLabel think = StaticData.currentThink();
                String txt = (cnt++ % 2 == 0) ? "Thinking" : "...";
                think.setText(txt);
                sleep(500);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
