package mvc.controller;

import java.io.File;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import jaxb.match.Data;
import mvc.model.Move;
import mvc.model.StaticData;

public class ReplayThread extends Thread {

    private final JFrame jFrame;

    public ReplayThread(JFrame jFrame) {
        this.jFrame = jFrame;
    }

    @Override
    public void run() {
        List<DefaultListModel> models = StaticData.clearModel();
        File dir = new File(StaticData.CURRENT_DIR);
        int cnt = dir.list().length;

        for (int i = 1; i <= cnt; i++) {
            try {
                String file = StaticData.CURRENT_DIR + "/" + i;
                Data data = JAXBParser.readData(new File(file));
                models.get(1 - i % 2).addElement(data.getMove());

                StaticData.convertXml(data.getName(), data.getCell());
                StaticData.move = new Move(i + 1);
                sleep(StaticData.slider.getValue() * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        JOptionPane.showMessageDialog(jFrame, "Replay has finished");
    }
}
