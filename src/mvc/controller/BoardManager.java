package mvc.controller;

import java.io.File;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import jaxb.match.Data;
import mvc.model.Move;
import mvc.model.StaticData;

public class BoardManager {

    public static void initBoard() {
        String[][] arr = {{"wr", "wp", null, null, null, null, "bp", "br"},
        {"wn", "wp", null, null, null, null, "bp", "bn"},
        {"wb", "wp", null, null, null, null, "bp", "bb"},
        {"wq", "wp", null, null, null, null, "bp", "bq"},
        {"wk", "wp", null, null, null, null, "bp", "bk"},
        {"wb", "wp", null, null, null, null, "bp", "bb"},
        {"wn", "wp", null, null, null, null, "bp", "bn"},
        {"wr", "wp", null, null, null, null, "bp", "br"}};

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                JButton button = StaticData.CHESS_BOARD.get(i).get(j);
                SwingUtils.updateCell(button, arr[i][j]);
            }
        }
        StaticData.startThink();
    }

    public static void updateBoard() {
        int cnt = StaticData.move.getMoveCnt();
        String piece = StaticData.move.getStart().getToolTipText();
        SwingUtils.updateCell(StaticData.move.getEnd(), piece);
        SwingUtils.updateCell(StaticData.move.getStart(), null);

        JList jList = StaticData.LIST_VIEW.get(1 - cnt % 2);
        DefaultListModel model = (DefaultListModel) jList.getModel();
        String next = String.valueOf(cnt) + ". " + piece.charAt(1)
                + StaticData.move;
        model.addElement(next);

        File file = new File(StaticData.CURRENT_DIR + "/" + cnt);
        JAXBParser.writeData(file, cnt, next);
        StaticData.currentThink().setText("Thinking");
        StaticData.move = new Move(cnt + 1);
    }

    public static void resetBoard(int name) {
        List<DefaultListModel> models = StaticData.clearModel();
        for (int i = 1; i <= name; i++) {
            String file = StaticData.CURRENT_DIR + "/" + i;
            Data data = JAXBParser.readData(new File(file));
            models.get(1 - i % 2).addElement(data.getMove());

            if (name == i) {
                StaticData.convertXml(data.getName(), data.getCell());
                StaticData.move = new Move(i + 1);
            }
        }
        StaticData.startThink();
    }
}
