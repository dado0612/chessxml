package mvc.controller;

import java.io.File;
import javax.swing.JButton;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import jaxb.board.dialog.Dialog;
import jaxb.board.frame.Frame;
import jaxb.match.Cell;
import jaxb.match.Data;
import mvc.model.StaticData;

public class JAXBParser {

    private static Data convertData(int id, String move) {
        Data data = new Data(id, StaticData.currentName(), move);
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                JButton button = StaticData.CHESS_BOARD.get(i).get(j);
                data.getCell().add(new Cell(i, j, button.getToolTipText()));
            }
        }
        return data;
    }

    public static void writeData(File file, int id, String move) {
        try {
            Data data = convertData(id, move);
            JAXBContext context = JAXBContext.newInstance(Data.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.marshal(data, file);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public static Data readData(File file) {
        try {
            JAXBContext context = JAXBContext.newInstance(Data.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            return (Data) unmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Dialog readDialog() {
        try {
            JAXBContext context = JAXBContext.newInstance(Dialog.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            File file = new File("xml/dialog.xml");
            return (Dialog) unmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Frame readFrame() {
        try {
            JAXBContext context = JAXBContext.newInstance(Frame.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            File file = new File("xml/frame.xml");
            return (Frame) unmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }
}
