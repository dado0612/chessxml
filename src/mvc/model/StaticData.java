package mvc.model;

import java.awt.Image;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JSlider;
import jaxb.match.Cell;
import mvc.controller.SwingUtils;
import mvc.controller.ThinkThread;

public class StaticData {

    private static final int IMG_SIZE = 35;
    public static final String DEFAULT_DIR = "sample/";
    public static final Map<String, Object> MAP_COMP = new HashMap<>();
    public static final Map<String, ImageIcon> MAP_PIECE = new HashMap<>();

    public static final List<List<JButton>> CHESS_BOARD = new ArrayList<>();
    public static final List<JLabel> LABEL_NAME = new ArrayList<>();
    public static final List<JLabel> LABEL_THINK = new ArrayList<>();
    public static final List<JList> LIST_VIEW = new ArrayList<>();

    public static JSlider slider;
    public static String CURRENT_DIR;
    public static Move move = new Move(1);
    public static ThinkThread thinkThread = new ThinkThread();

    private static ImageIcon getIcon(String file) {
        Image img = new ImageIcon(file).getImage();
        Image icon = img.getScaledInstance(IMG_SIZE, IMG_SIZE, Image.SCALE_SMOOTH);
        return (new ImageIcon(icon));
    }

    static {
        for (int i = 0; i < 8; i++) {
            List<JButton> list = new ArrayList<>(8);
            CHESS_BOARD.add(list);
        }

        MAP_PIECE.put("bb", getIcon("img/bb.png"));
        MAP_PIECE.put("bk", getIcon("img/bk.png"));
        MAP_PIECE.put("bn", getIcon("img/bn.png"));
        MAP_PIECE.put("bp", getIcon("img/bp.png"));
        MAP_PIECE.put("bq", getIcon("img/bq.png"));
        MAP_PIECE.put("br", getIcon("img/br.png"));

        MAP_PIECE.put("wb", getIcon("img/wb.png"));
        MAP_PIECE.put("wk", getIcon("img/wk.png"));
        MAP_PIECE.put("wn", getIcon("img/wn.png"));
        MAP_PIECE.put("wp", getIcon("img/wp.png"));
        MAP_PIECE.put("wq", getIcon("img/wq.png"));
        MAP_PIECE.put("wr", getIcon("img/wr.png"));
    }

    public static void convertXml(String name, List<Cell> list) {
        String[] arr = name.split("[,]");
        for (int i = 0; i < 2; i++) {
            LABEL_NAME.get(i + 1).setText(arr[i]);
        }

        for (int i = 0; i < 64; i++) {
            JButton button = CHESS_BOARD.get(i / 8).get(i % 8);
            SwingUtils.updateCell(button, list.get(i).getPiece());
        }
    }

    public static JLabel currentThink() {
        return (LABEL_THINK.get(1 - move.getMoveCnt() % 2));
    }

    public static String currentName() {
        return (LABEL_NAME.get(1).getText() + "," + LABEL_NAME.get(2).getText());
    }

    public static void startThink() {
        thinkThread.interrupt();
        thinkThread = new ThinkThread();

        LABEL_THINK.forEach((label) -> {
            label.setText("Thinking");
        });
        thinkThread.start();
    }

    public static List<DefaultListModel> clearModel() {
        List<DefaultListModel> models = new ArrayList<>();
        LIST_VIEW.stream().map((list) -> (DefaultListModel) list.getModel()).map((model) -> {
            model.clear();
            return model;
        }).forEachOrdered((model) -> {
            models.add(model);
        });
        return models;
    }

    public static void changeState(boolean flag) {
        LABEL_THINK.forEach((label) -> {
            label.setVisible(flag);
        });
        slider.setVisible(!flag);
    }
}
