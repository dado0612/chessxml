package mvc.model;

import javax.swing.JButton;

public class Move {

    private int moveCnt;
    private String from;
    private String to;
    private JButton start;
    private JButton end;

    public Move(int moveCnt) {
        this.moveCnt = moveCnt;
    }

    public int getMoveCnt() {
        return moveCnt;
    }

    public void setMoveCnt(int moveCnt) {
        this.moveCnt = moveCnt;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public JButton getStart() {
        return start;
    }

    public void setStart(JButton start) {
        this.start = start;
    }

    public JButton getEnd() {
        return end;
    }

    public void setEnd(JButton end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return from + " - " + to;
    }
}
