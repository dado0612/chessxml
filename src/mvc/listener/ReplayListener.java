package mvc.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import mvc.controller.ReplayThread;
import mvc.model.StaticData;

public class ReplayListener implements ActionListener {

    private final JFrame frame;

    public ReplayListener(JFrame frame) {
        this.frame = frame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser chooser = new JFileChooser(new File(StaticData.DEFAULT_DIR));
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        if (chooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
            File dir = chooser.getSelectedFile();
            StaticData.CURRENT_DIR = dir.getPath();
            StaticData.changeState(false);
            new ReplayThread(frame).start();
        }
    }
}
