package mvc.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import mvc.controller.BoardManager;
import mvc.model.StaticData;

public class ContinueListener implements ActionListener {

    private final JFrame frame;

    public ContinueListener(JFrame frame) {
        this.frame = frame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser chooser = new JFileChooser(new File(StaticData.DEFAULT_DIR));
        if (chooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile();
            File dir = chooser.getCurrentDirectory();

            StaticData.CURRENT_DIR = dir.getPath();
            StaticData.changeState(true);
            BoardManager.resetBoard(Integer.valueOf(file.getName()));
        }
    }
}
