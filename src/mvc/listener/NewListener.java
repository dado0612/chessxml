package mvc.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Date;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JTextField;
import mvc.controller.BoardManager;
import mvc.model.Move;
import mvc.model.StaticData;

public class NewListener implements ActionListener {

    private final List<JTextField> textFields;
    private final JDialog dialog;

    public NewListener(List<JTextField> textFields, JDialog dialog) {
        this.textFields = textFields;
        this.dialog = dialog;
    }

    private boolean makeDir() {
        long timeStamp = (new Date()).getTime();
        StaticData.CURRENT_DIR = StaticData.DEFAULT_DIR + timeStamp;
        return (new File(StaticData.CURRENT_DIR)).mkdir();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        StaticData.move = new Move(1);
        boolean flag = makeDir();

        BoardManager.initBoard();
        StaticData.clearModel();
        StaticData.changeState(true);

        for (int i = 0; i < 2; i++) {
            String name = textFields.get(i).getText();
            StaticData.LABEL_NAME.get(i + 1).setText(name);
        }
        dialog.dispose();
    }
}
