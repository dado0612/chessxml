package mvc.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTextField;
import jaxb.board.dialog.Dialog;
import mvc.controller.JAXBParser;
import mvc.view.CreateDialog;

public class StartListener implements ActionListener {

    private final JFrame frame;

    public StartListener(JFrame frame) {
        this.frame = frame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Dialog dialog = JAXBParser.readDialog();
        JDialog jDialog = CreateDialog.createComponent(dialog);
        dialog.getLabel().forEach((label) -> {
            jDialog.add(CreateDialog.createComponent(label));
        });

        List<JTextField> textFields = new ArrayList<>();
        dialog.getTextField().stream().map((textField) -> CreateDialog.createComponent(textField)).map((jTextField) -> {
            jDialog.add(jTextField);
            return jTextField;
        }).forEachOrdered((jTextField) -> {
            textFields.add(jTextField);
        });

        JButton jButton = CreateDialog.createComponent(dialog.getButton());
        jButton.addActionListener(new NewListener(textFields, jDialog));
        jDialog.add(jButton);

        jDialog.setModal(true);
        jDialog.setLocationRelativeTo(frame);
        jDialog.setVisible(true);
    }
}
