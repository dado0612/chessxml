package mvc.listener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JList;
import mvc.controller.BoardManager;

public class ListListener implements MouseListener {

    private final JList list;

    public ListListener(JList list) {
        this.list = list;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() > 1) {
            String val = list.getSelectedValue().toString();
            String[] arr = val.split("[.]");
            BoardManager.resetBoard(Integer.valueOf(arr[0]));
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
