package mvc.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import mvc.controller.BoardManager;
import mvc.model.StaticData;

public class MoveListener implements ActionListener {

    private final String cell;
    private final JButton button;

    public MoveListener(String cell, JButton button) {
        this.cell = cell;
        this.button = button;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (StaticData.move.getFrom() == null) {
            String piece = button.getToolTipText();
            String need = (StaticData.move.getMoveCnt() % 2 == 0) ? "b" : "w";

            if ((piece != null) && (piece.startsWith(need))) {
                StaticData.move.setFrom(cell);
                StaticData.move.setStart(button);
            }
        } else {
            StaticData.move.setTo(cell);
            StaticData.move.setEnd(button);
            BoardManager.updateBoard();
        }
    }
}
