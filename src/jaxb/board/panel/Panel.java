package jaxb.board.panel;

public class Panel {

    private String location;
    private String size;
    private String layout;

    public Panel(PanelBoard board) {
        this.location = board.getLocation();
        this.size = board.getSize();
        this.layout = board.getLayout();
    }

    public Panel(PanelMatch match) {
        this.location = match.getLocation();
        this.size = match.getSize();
    }

    public Panel(PanelMenu menu) {
        this.location = menu.getLocation();
        this.size = menu.getSize();
        this.layout = menu.getLayout();
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    @Override
    public String toString() {
        return "Panel{" + "location=" + location + ", size=" + size + ", layout=" + layout + '}';
    }
}
