package jaxb.board.panel;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import jaxb.board.dialog.Button;

@XmlRootElement
public class PanelBoard implements Serializable {

    private String location;
    private String size;
    private String layout;
    private List<Button> button;

    public PanelBoard() {
    }

    public String getLocation() {
        return location;
    }

    @XmlElement
    public void setLocation(String location) {
        this.location = location;
    }

    public String getSize() {
        return size;
    }

    @XmlElement
    public void setSize(String size) {
        this.size = size;
    }

    public String getLayout() {
        return layout;
    }

    @XmlElement
    public void setLayout(String layout) {
        this.layout = layout;
    }

    public List<Button> getButton() {
        return button;
    }

    @XmlElement
    public void setButton(List<Button> button) {
        this.button = button;
    }

    @Override
    public String toString() {
        return "PanelBoard{" + "location=" + location + ", size=" + size + ", layout=" + layout + ", button=" + button + '}';
    }
}
