package jaxb.board.panel;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import jaxb.board.frame.RadioButton;

@XmlRootElement
public class PanelMenu implements Serializable {

    private String location;
    private String size;
    private String layout;
    private List<RadioButton> radioButton;

    public PanelMenu() {
    }

    public String getLocation() {
        return location;
    }

    @XmlElement
    public void setLocation(String location) {
        this.location = location;
    }

    public String getSize() {
        return size;
    }

    @XmlElement
    public void setSize(String size) {
        this.size = size;
    }

    public String getLayout() {
        return layout;
    }

    @XmlElement
    public void setLayout(String layout) {
        this.layout = layout;
    }

    public List<RadioButton> getRadioButton() {
        return radioButton;
    }

    @XmlElement
    public void setRadioButton(List<RadioButton> radioButton) {
        this.radioButton = radioButton;
    }

    @Override
    public String toString() {
        return "PanelMenu{" + "location=" + location + ", size=" + size + ", layout=" + layout + ", radioButton=" + radioButton + '}';
    }
}
