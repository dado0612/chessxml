package jaxb.board.panel;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import jaxb.board.dialog.Label;
import jaxb.board.dialog.Slider;
import jaxb.board.frame.ScrollPane;

@XmlRootElement
public class PanelMatch implements Serializable {

    private String location;
    private String size;

    private List<Label> label;
    private List<ScrollPane> scrollPane;
    private Slider slider;

    public PanelMatch() {
    }

    public String getLocation() {
        return location;
    }

    @XmlElement
    public void setLocation(String location) {
        this.location = location;
    }

    public String getSize() {
        return size;
    }

    @XmlElement
    public void setSize(String size) {
        this.size = size;
    }

    public List<Label> getLabel() {
        return label;
    }

    @XmlElement
    public void setLabel(List<Label> label) {
        this.label = label;
    }

    public List<ScrollPane> getScrollPane() {
        return scrollPane;
    }

    @XmlElement
    public void setScrollPane(List<ScrollPane> scrollPane) {
        this.scrollPane = scrollPane;
    }

    public Slider getSlider() {
        return slider;
    }

    @XmlElement
    public void setSlider(Slider slider) {
        this.slider = slider;
    }

    @Override
    public String toString() {
        return "PanelMatch{" + "location=" + location + ", size=" + size + ", label=" + label + ", scrollPane=" + scrollPane + ", slider=" + slider + '}';
    }
}
