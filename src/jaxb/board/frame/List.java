package jaxb.board.frame;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class List implements Serializable {

    private String name;
    private int selectionMode;

    public List() {
    }

    public String getName() {
        return name;
    }

    @XmlAttribute
    public void setName(String name) {
        this.name = name;
    }

    public int getSelectionMode() {
        return selectionMode;
    }

    @XmlElement
    public void setSelectionMode(int selectionMode) {
        this.selectionMode = selectionMode;
    }

    @Override
    public String toString() {
        return "List{" + "name=" + name + ", selectionMode=" + selectionMode + '}';
    }
}
