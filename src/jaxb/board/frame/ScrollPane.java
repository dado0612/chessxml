package jaxb.board.frame;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ScrollPane implements Serializable {

    private String name;
    private String location;
    private String size;
    private List list;

    public ScrollPane() {
    }

    public String getName() {
        return name;
    }

    @XmlAttribute
    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    @XmlElement
    public void setLocation(String location) {
        this.location = location;
    }

    public String getSize() {
        return size;
    }

    @XmlElement
    public void setSize(String size) {
        this.size = size;
    }

    public List getList() {
        return list;
    }

    @XmlElement
    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "ScrollPane{" + "name=" + name + ", location=" + location + ", size=" + size + ", list=" + list + '}';
    }
}
