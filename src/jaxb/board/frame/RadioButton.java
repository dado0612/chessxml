package jaxb.board.frame;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RadioButton implements Serializable {

    private String name;
    private String font;
    private String text;
    private int horizontalAlignment;

    public RadioButton() {
    }

    public String getName() {
        return name;
    }

    @XmlAttribute
    public void setName(String name) {
        this.name = name;
    }

    public String getFont() {
        return font;
    }

    @XmlElement
    public void setFont(String font) {
        this.font = font;
    }

    public String getText() {
        return text;
    }

    @XmlElement
    public void setText(String text) {
        this.text = text;
    }

    public int getHorizontalAlignment() {
        return horizontalAlignment;
    }

    @XmlElement
    public void setHorizontalAlignment(int horizontalAlignment) {
        this.horizontalAlignment = horizontalAlignment;
    }

    @Override
    public String toString() {
        return "RadioButton{" + "name=" + name + ", font=" + font + ", text=" + text + ", horizontalAlignment=" + horizontalAlignment + '}';
    }
}
