package jaxb.board.frame;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import jaxb.board.panel.PanelBoard;
import jaxb.board.panel.PanelMatch;
import jaxb.board.panel.PanelMenu;

@XmlRootElement
public class Frame implements Serializable {

    private String size;
    private int defaultCloseOperation;
    private String layout;
    private String title;
    private boolean resizable;

    private PanelBoard panelBoard;
    private PanelMatch panelMatch;
    private PanelMenu panelMenu;

    public Frame() {
    }

    public String getSize() {
        return size;
    }

    @XmlElement
    public void setSize(String size) {
        this.size = size;
    }

    public int getDefaultCloseOperation() {
        return defaultCloseOperation;
    }

    @XmlElement
    public void setDefaultCloseOperation(int defaultCloseOperation) {
        this.defaultCloseOperation = defaultCloseOperation;
    }

    public String getLayout() {
        return layout;
    }

    @XmlElement
    public void setLayout(String layout) {
        this.layout = layout;
    }

    public String getTitle() {
        return title;
    }

    @XmlElement
    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isResizable() {
        return resizable;
    }

    @XmlElement
    public void setResizable(boolean resizable) {
        this.resizable = resizable;
    }

    public PanelBoard getPanelBoard() {
        return panelBoard;
    }

    @XmlElement
    public void setPanelBoard(PanelBoard panelBoard) {
        this.panelBoard = panelBoard;
    }

    public PanelMatch getPanelMatch() {
        return panelMatch;
    }

    @XmlElement
    public void setPanelMatch(PanelMatch panelMatch) {
        this.panelMatch = panelMatch;
    }

    public PanelMenu getPanelMenu() {
        return panelMenu;
    }

    @XmlElement
    public void setPanelMenu(PanelMenu panelMenu) {
        this.panelMenu = panelMenu;
    }

    @Override
    public String toString() {
        return "Frame{" + "size=" + size + ", defaultCloseOperation=" + defaultCloseOperation + ", layout=" + layout + ", title=" + title + ", resizable=" + resizable + ", panelBoard=" + panelBoard + ", panelMatch=" + panelMatch + ", panelMenu=" + panelMenu + '}';
    }
}
