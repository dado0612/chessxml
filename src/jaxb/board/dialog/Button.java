package jaxb.board.dialog;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Button implements Serializable {

    private String name;
    private String location;
    private String size;
    private String background;
    private int horizontalTextPosition;
    private String margin;
    private String text;

    public Button() {
    }

    public String getName() {
        return name;
    }

    @XmlAttribute
    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    @XmlElement
    public void setLocation(String location) {
        this.location = location;
    }

    public String getSize() {
        return size;
    }

    @XmlElement
    public void setSize(String size) {
        this.size = size;
    }

    public String getBackground() {
        return background;
    }

    @XmlElement
    public void setBackground(String background) {
        this.background = background;
    }

    public int getHorizontalTextPosition() {
        return horizontalTextPosition;
    }

    @XmlElement
    public void setHorizontalTextPosition(int horizontalTextPosition) {
        this.horizontalTextPosition = horizontalTextPosition;
    }

    public String getMargin() {
        return margin;
    }

    @XmlElement
    public void setMargin(String margin) {
        this.margin = margin;
    }

    public String getText() {
        return text;
    }

    @XmlElement
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Button{" + "name=" + name + ", location=" + location + ", size=" + size + ", background=" + background + ", horizontalTextPosition=" + horizontalTextPosition + ", margin=" + margin + ", text=" + text + '}';
    }
}
