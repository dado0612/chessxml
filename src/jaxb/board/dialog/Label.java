package jaxb.board.dialog;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Label implements Serializable {

    private String name;
    private String location;
    private String size;
    private String font;
    private String foreground;
    private int horizontalAlignment;
    private String text;

    public Label() {
    }

    public String getName() {
        return name;
    }

    @XmlAttribute
    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    @XmlElement
    public void setLocation(String location) {
        this.location = location;
    }

    public String getSize() {
        return size;
    }

    @XmlElement
    public void setSize(String size) {
        this.size = size;
    }

    public String getFont() {
        return font;
    }

    @XmlElement
    public void setFont(String font) {
        this.font = font;
    }

    public String getForeground() {
        return foreground;
    }

    @XmlElement
    public void setForeground(String foreground) {
        this.foreground = foreground;
    }

    public int getHorizontalAlignment() {
        return horizontalAlignment;
    }

    @XmlElement
    public void setHorizontalAlignment(int horizontalAlignment) {
        this.horizontalAlignment = horizontalAlignment;
    }

    public String getText() {
        return text;
    }

    @XmlElement
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Label{" + "name=" + name + ", location=" + location + ", size=" + size + ", font=" + font + ", foreground=" + foreground + ", horizontalAlignment=" + horizontalAlignment + ", text=" + text + '}';
    }
}
