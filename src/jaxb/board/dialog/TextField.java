package jaxb.board.dialog;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TextField implements Serializable {

    private String name;
    private String location;
    private String size;

    public TextField() {
    }

    public String getName() {
        return name;
    }

    @XmlAttribute
    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    @XmlElement
    public void setLocation(String location) {
        this.location = location;
    }

    public String getSize() {
        return size;
    }

    @XmlElement
    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "TextField{" + "name=" + name + ", location=" + location + ", size=" + size + '}';
    }
}
