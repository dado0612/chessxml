package jaxb.board.dialog;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Slider implements Serializable {

    private String name;
    private String location;
    private String size;
    private int majorTickSpacing;
    private int maximum;
    private int minimum;
    private boolean paintLabels;
    private boolean paintTicks;

    public Slider() {
    }

    public String getName() {
        return name;
    }

    @XmlAttribute
    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    @XmlElement
    public void setLocation(String location) {
        this.location = location;
    }

    public String getSize() {
        return size;
    }

    @XmlElement
    public void setSize(String size) {
        this.size = size;
    }

    public int getMajorTickSpacing() {
        return majorTickSpacing;
    }

    @XmlElement
    public void setMajorTickSpacing(int majorTickSpacing) {
        this.majorTickSpacing = majorTickSpacing;
    }

    public int getMaximum() {
        return maximum;
    }

    @XmlElement
    public void setMaximum(int maximum) {
        this.maximum = maximum;
    }

    public int getMinimum() {
        return minimum;
    }

    @XmlElement
    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    public boolean isPaintLabels() {
        return paintLabels;
    }

    @XmlElement
    public void setPaintLabels(boolean paintLabels) {
        this.paintLabels = paintLabels;
    }

    public boolean isPaintTicks() {
        return paintTicks;
    }

    @XmlElement
    public void setPaintTicks(boolean paintTicks) {
        this.paintTicks = paintTicks;
    }

    @Override
    public String toString() {
        return "Slider{" + "name=" + name + ", location=" + location + ", size=" + size + ", majorTickSpacing=" + majorTickSpacing + ", maximum=" + maximum + ", minimum=" + minimum + ", paintLabels=" + paintLabels + ", paintTicks=" + paintTicks + '}';
    }
}
