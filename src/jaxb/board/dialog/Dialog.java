package jaxb.board.dialog;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Dialog implements Serializable {

    private String size;
    private int defaultCloseOperation;
    private String layout;
    private String title;
    private boolean resizable;

    private Button button;
    private List<Label> label;
    private List<TextField> textField;

    public Dialog() {
    }

    public String getSize() {
        return size;
    }

    @XmlElement
    public void setSize(String size) {
        this.size = size;
    }

    public int getDefaultCloseOperation() {
        return defaultCloseOperation;
    }

    @XmlElement
    public void setDefaultCloseOperation(int defaultCloseOperation) {
        this.defaultCloseOperation = defaultCloseOperation;
    }

    public String getLayout() {
        return layout;
    }

    @XmlElement
    public void setLayout(String layout) {
        this.layout = layout;
    }

    public String getTitle() {
        return title;
    }

    @XmlElement
    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isResizable() {
        return resizable;
    }

    @XmlElement
    public void setResizable(boolean resizable) {
        this.resizable = resizable;
    }

    public Button getButton() {
        return button;
    }

    @XmlElement
    public void setButton(Button button) {
        this.button = button;
    }

    public List<Label> getLabel() {
        return label;
    }

    @XmlElement
    public void setLabel(List<Label> label) {
        this.label = label;
    }

    public List<TextField> getTextField() {
        return textField;
    }

    @XmlElement
    public void setTextField(List<TextField> textField) {
        this.textField = textField;
    }

    @Override
    public String toString() {
        return "Dialog{" + "size=" + size + ", defaultCloseOperation=" + defaultCloseOperation + ", layout=" + layout + ", title=" + title + ", resizable=" + resizable + ", button=" + button + ", label=" + label + ", textField=" + textField + '}';
    }
}
