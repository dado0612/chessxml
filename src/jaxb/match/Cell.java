package jaxb.match;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Cell implements Serializable {

    private int row;
    private int col;
    private String piece;

    public Cell() {
    }

    public Cell(int row, int col, String piece) {
        this.row = row;
        this.col = col;
        this.piece = piece;
    }

    public int getRow() {
        return row;
    }

    @XmlAttribute
    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    @XmlAttribute
    public void setCol(int col) {
        this.col = col;
    }

    public String getPiece() {
        return piece;
    }

    @XmlElement
    public void setPiece(String piece) {
        this.piece = piece;
    }

    @Override
    public String toString() {
        return "Cell{" + "row=" + row + ", col=" + col + ", piece=" + piece + '}';
    }
}
