package jaxb.match;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Data implements Serializable {

    private int id;
    private String name;
    private String move;
    private List<Cell> cell;

    {
        cell = new ArrayList<>();
    }

    public Data() {
    }

    public Data(int id, String name, String move) {
        this.id = id;
        this.name = name;
        this.move = move;
    }

    public int getId() {
        return id;
    }

    @XmlAttribute
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @XmlElement
    public void setName(String name) {
        this.name = name;
    }

    public String getMove() {
        return move;
    }

    @XmlElement
    public void setMove(String move) {
        this.move = move;
    }

    public List<Cell> getCell() {
        return cell;
    }

    @XmlElement
    public void setCell(List<Cell> cell) {
        this.cell = cell;
    }

    @Override
    public String toString() {
        return "Data{" + "id=" + id + ", name=" + name + ", move=" + move + ", cell=" + cell + '}';
    }
}
